from ..eocohda.core.algorithm import SolutionRepairer
import numpy as np
import copy


class FlexSolutionRepairer(SolutionRepairer):
    """
        Sometimes you have a good solution which is not in the solution space
        of valid solutions. In this situation you often want to repair this
        solution without losing that much quality. This class implements some
        repair strategies used by the different algorithms.
        """

    def repair_limit(self, solution):
        """
        Clip load state

        :param solution: solution you want to repair
        :returns: solution
        """
        for i in range(len(solution.slots)):
            slot = solution.slots[i]
            slot.load_state = self.__clip_load(slot.load_state, i)
        return solution

    def __clip(self, v, lower, upper):
        return max(lower, min(v, upper))

    def __clip_load(self, load, i):
        return self.__clip(load, 
                           self._storage.min_allowed_soc_at(i), 
                           self._storage.max_allowed_soc_at(i))

    def offset_following(self, candidate, start, offset):
        """
        Increase all slot load_states with the given offset, starting at
        'start'
        :param candidate: schedule
        :param start: start index
        :param offset: offset to increase
        """
        for i in range(start, candidate.size()):
            candidate.slots[i].load_state = self.__clip_load(
                candidate.slots[i].load_state + offset, i)

    def repair_strategies(self, solution):
        return solution
        
    def repair_max_power(self, solution):
        """
        Repair load states with respect to the max charge/discharge constraint.

        :param solution: solution you want to repair
        :returns: solution
        """
        storage_copy = self._copy_storage
        for i in range(solution.size()):
            prev = \
                self._storage.load if i == 0 else solution.slots[i -
                                                                 1].load_state
            current = solution.slots[i].load_state
            storage_copy.load = prev
            load_min_max = storage_copy.possible_load_one_step_at(i)
            solution.slots[i].load_state = \
                self.__clip(current, load_min_max[0], load_min_max[1])
        return solution
