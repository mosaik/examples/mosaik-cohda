import uuid
import asyncio
import logging

from mango import Role, RoleContext
from mango.util.scheduling import InstantScheduledTask

from .mango_library.negotiation.cohda import *
from .mango_library.negotiation.termination import \
    NegotiationTerminationRole, TerminationMessage
from .mango_library.negotiation.core import NegotiationMessage
from .mango_library.coalition.core import CoalitionInitiatorRole, \
    CoalitionAssignment, CoalitionModel, CoaltitionResponse, \
    CoalitionInvite, clique_creator

from .start_values import StartValues, SolutionSchedule, \
    FinalTermination

from .des_flex.flex_class import Flexibility
from .eocohda.algorithm.stopping import createDefaultStopping 
from .flex_cohda.schedule_generator import ScheduleGenerator


class TerminationData:
    terminated: bool


class FlexReceiverRole(Role):
    """Receiver role class:
    The role receives new flexibilities and sent flex schedule back at the end
    """

    def __init__(self, mosaik_agent_address):
        super().__init__()
        self._mosaik_agent_address = mosaik_agent_address  # Tuple
        logging.info('Agent got created')
        # containing container and ID

    def setup(self) -> None:
        """Lifecycle hook in, which will be called on adding the role to
        agent. The role context is known from here on.
        """
        self.context.subscribe_message(self, self.handle_message,
                                       lambda c, m: isinstance(c, Flexibility))
        self.context.subscribe_model(self, TerminationData)

    def on_change_model(self, model) -> None:
        """Will be invoked when a subscribed model changes via
        :func:`RoleContext.update`.

        :param model: the model
        """
        if model.terminated:
            logging.info(
                'Agent %s trys to send schedule to mosaik agent %s, %s',
                self.context.aid, self._mosaik_agent_address[0],
                self._mosaik_agent_address[1])

            # extract schedule as list from cohda solution
            schedule = \
                self.context.data.current_cohda_solution.target_schedule
            logging.info(
                'Agent %s target_schedule= %s', self.context.aid,
                schedule)

            solution = SolutionSchedule(schedule)
            self.context.schedule_instant_task(
                self.context.send_acl_message(
                    receiver_addr=self._mosaik_agent_address[0],
                    receiver_id=self._mosaik_agent_address[1],
                    content=solution,
                    acl_metadata={'conversation_id':
                                      self._mosaik_agent_address[1],
                                  'sender_id': self.context.aid},
                    
                )
            )

    def handle_message(self, content, meta: Dict[str, Any]) -> None:
        """
               Handling all incoming messages of the agent
               :param content: The message content
               :param meta: All meta information in a dict.
               Possible keys are e. g. performative, sender_addr, sender_id,
               conversation_id...
               """

        logging.info('Agent %s got new flex', self.context.aid)
        self.context.data.flex = content


class FlexNegotiationStarterRole(CoalitionInitiatorRole):
    """Role responsible for initiating a coalition.

    The role will invite all given participants and add them to coalition if
    they accept the invite.

    Also this role will start the negotiation
    """

    def __init__(self, topic: str = 'cohda', details: str = 'flex_cohda',
                 topology_creator=clique_creator):
        self._topic = topic
        self._details = details
        self._topology_creator = topology_creator
        self._part_to_state = {}
        self._assignments_sent = False
        self._coal_id = None

    def setup(self):
        # subscriptions
        self.context.subscribe_message(
            self, self.handle_message, lambda c, m: isinstance(c,
                                                           CoaltitionResponse))

        self.context.subscribe_message(
            self, self.handle_msg_start_values,
            lambda c, m: isinstance(c, StartValues))

        self.context.subscribe_model(self, TerminationData)

    async def send_invitiations(self, agent_context: RoleContext):
        """Send invitations to all wanted participant for the coalition

        :param agent_context: the context
        """
        self._coal_id = uuid.uuid1()

        for participant in self._participants:
            await agent_context.send_acl_message(
                content=CoalitionInvite(self._coal_id, self._topic),
                receiver_addr=participant[0],
                receiver_id=participant[1],
                acl_metadata={'sender_addr': agent_context.addr,
                              'sender_id': agent_context.aid},
                )

    def handle_message(self, content: CoaltitionResponse, meta: Dict[str, Any]) \
            -> None:
        """Handle the responses to the invites.

        :param content: the invite response
        :param meta: meta data
        """
        self._part_to_state[(meta['sender_addr'],
                             meta['sender_id'])] = content.accept

        if len(self._part_to_state) == len(self._participants) \
                and not self._assignments_sent:
            self._send_assignments(self.context)
            self._assignments_sent = True

    def _send_assignments(self, agent_context: RoleContext):
        part_id = 0
        self._accepted_participants = []
        for part in self._participants:
            if part in self._part_to_state:
                if self._part_to_state[part]:
                    part_id += 1
                    self._accepted_participants.append((part_id, part[0],
                                                        part[1]))

        part_to_neighbors = self._topology_creator(self._accepted_participants)
        for part in self._accepted_participants:
            asyncio.create_task(agent_context.send_acl_message(
                content=CoalitionAssignment(self._coal_id,
                                            part_to_neighbors[part],
                                            self._topic, part[0],
                                            agent_context.aid,
                                            agent_context.addr),
                receiver_addr=part[1], receiver_id=part[2],
                acl_metadata={'sender_addr': agent_context.addr,
                              'sender_id': agent_context.aid},
                ))

    def on_change_model(self, model) -> None:
        """Will be invoked when a subscribed model changes via
        :func:`RoleContext.update`.

        :param model: the model
        """
        if model.terminated:
            logging.info(
                'Agent %s trys to send termination to all other agents')

            # extract schedule as list from cohda solution
            for part in self._accepted_participants:
                termination = FinalTermination(True)
                self.context.schedule_instant_task(
                    self.context.send_acl_message(
                        receiver_addr=part[1],
                        receiver_id=part[2],
                        content=termination,
                        acl_metadata={'sender_addr': self.context.addr,
                                      'sender_id': self.context.aid},
                        ))

    def handle_msg_start_values(self, content, meta: Dict[str, Any]) -> None:
        """
        Handling all incoming messages of the agent
        :param content: The message content
        :param meta: All meta information in a dict.
        Possible keys are e. g. performative, sender_addr, sender_id,
        conversation_id...
        """

        logging.info('Agent %s got new start values', self.context.aid)

        self._participants = []
        # tuples got serialized to lists, undo
        self.context.data.start_values = content

        for participant in content.participants:
            self._participants.append((tuple(participant[0]), participant[1]))

        self.context.data.start_values.participants = self._participants
        logging.info('Got start values: Schedule Invitations with '
                     'participants: %s', self._participants)
        self.context.schedule_task(InstantScheduledTask(
            self.send_invitiations(self.context)))

        self.context.schedule_conditional_task(
            self.start(), lambda: self._assignments_sent and
                                  self.context.get_or_create_model(
                                      CoalitionModel).assignments)

    async def start(self):
        """Start a negotiation. Send all neighbors a starting negotiation
        message.
        """
        logging.info('Ask agents to start negotiation')
        coalition_model = self.context.get_or_create_model(CoalitionModel)

        # Assume there is a exactly one coalition
        matched_assignment = coalition_model.by_id(self._coal_id)
        if matched_assignment:
            negotiation_uuid = uuid.uuid1()
            logging.info('Agent %s sends schedule %s',
                        self.context.aid, self.context.data.start_values.schedule)
            for neighbor in matched_assignment.neighbors:
                await self.context.send_acl_message(
                    content=
                    NegotiationMessage(
                        matched_assignment.coalition_id, negotiation_uuid,
                        CohdaMessage(WorkingMemory(
                            self.context.data.start_values.schedule, {},
                            SolutionCandidate(matched_assignment.part_id, {})))),
                    receiver_addr=neighbor[1],
                    receiver_id=neighbor[2],
                    acl_metadata={'sender_addr': self.context.addr, 'sender_id':
                        self.context.aid},
                    )


class FlexCohdaRole(COHDARole):
    """Negotiation role for COHDA.
    This role extends the cohda role by adding functionalities to support
    des_flex as input
    """

    def __init__(self, weights=1, local_acceptable_func=lambda s: True,
                 time_resolution=15 * 60):
        super().__init__(schedules_provider=None, weights=weights,
                         local_acceptable_func=local_acceptable_func)
        self._cohda: COHDA
        self._schedule_generator: ScheduleGenerator
        self._schedules: list[float]
        self._time_resolution = time_resolution

        # 5 = number of times the schedule generation will be triggered, it is not
        # the overall iteration count
        self._stop_crit = createDefaultStopping(None, 10)

    def create_cohda(self, part_id: int):
        """Create an instance of the COHDA-decider.

        :param part_id: participant id

        :return: COHDA
        """
        logging.info('Agent %s starts schedule generating for first time',
                     self.context.aid)
        self._schedule_generator = \
            ScheduleGenerator(flexibility=self.context.data.flex,
                              time_resolution=self._time_resolution)
        self._schedules = self._schedule_generator.create_schedules()
        self._schedules_provider = lambda: self._schedules

        logging.info('Agent %s finished generating schedule generating for '
                     'first time', self.context.aid)

        return COHDA(self._weights, self._schedules_provider,
                     self._is_local_acceptable, part_id)

    def handle(self,
               message,
               assignment: CoalitionAssignment,
               negotiation: Negotiation,
               meta: Dict[str, Any]):

        if negotiation.coalition_id not in self._cohda:
            self._cohda[negotiation.coalition_id] = self.create_cohda(
                assignment.part_id)

        (old, new) = self._cohda[negotiation.coalition_id].decide(message)
        self.context.data.current_cohda_solution = new

        if old != new:
            self.send_to_neighbors(assignment, negotiation, CohdaMessage(new))

            if not self._stop_crit.stop(
                    self._schedule_generator.current_solutions()):
                self._schedule_generator.update_cohda_objective(
                    lambda solution:
                    self.objective_function_one_agent(solution, new,
                                                      assignment.part_id))
                logging.info('Agent %s starts updating possible schedules',
                             self.context.aid)
                self._schedules = self._schedule_generator.create_schedules()

            logging.info('Agent %s updated possible schedules',
                         self.context.aid)
            # the schedules will be updated for each new cohda objective
            # function
            # self.context.data.current_cohda_solution = \
            #     new.working_candidate.solution_candidate.candidate[
            #         assignment.part_id]

            # set agent as idle
            if self.context.inbox_length() == 0:
                negotiation.active = False

    def objective_function_one_agent(self, schedule, memory, part_id):
        """Objective function of COHDA. Calculates the negative sum of all
        deviations of the candidate to the target schedule

        :param schedule: schedule from flex-cohda
        :param memory: memory of last cohda computation
        :param part_id: part_id to place new schedule at right position

        :return: negative sum of all deviations
        """
        target_schedule = memory.target_schedule
        candidate_schedules = memory.solution_candidate.candidate
        old_schedule = candidate_schedules[part_id]
        candidate_schedules[part_id] = schedule
        # Return the negative(!) sum of all deviations, because bigger scores
        # mean better plans (e.g., -1 is better then -10).
        # print('objective_function: ')
        cluster_schedule = np.array(
            list(map(lambda item: item[1], candidate_schedules.items())))
        sum_cs = cluster_schedule.sum(axis=0)  # sum for each interval
        diff = np.abs(
            target_schedule - sum_cs)  # deviation to the target schedule
        result = -np.sum(diff)
        candidate_schedules[part_id] = old_schedule
        return result

    def objective_function(self, memory):
        """Objective function of COHDA. Calculates the negative sum of all
        deviations of the candidate to the target schedule

        :param candidate: candidate schedule
        :param target_schedule: target schedule

        :return: negative sum of all deviations
        """
        target_schedule = memory.target_schedule
        candidate = memory.solution_candidate.candidate
        # Return the negative(!) sum of all deviations, because bigger scores
        # mean better plans (e.g., -1 is better then -10).
        # print('objective_function: ')
        cluster_schedule = np.array(list(map(lambda item: item[1],
                                             candidate.items())))
        if not cluster_schedule.any():
            return -300
        if cluster_schedule[0] is None:
            return -300
        logging.info('cluster_schedule: %s', cluster_schedule)
        sum_cs = cluster_schedule.sum(axis=0)  # sum for each interval
        diff = np.abs(
            target_schedule - sum_cs)  # deviation to the target schedule
        result = -np.sum(diff)
        return result


class FlexTerminationRole(NegotiationTerminationRole):
    """Role for negotiation participants. Will add the weight attribute to
    every coalition related message send.
    This role will also inform all agents when the negotiation is terminated
    """

    def __init__(self, is_controller: bool = 0) -> None:
        super().__init__()
        self._weight_map = {}
        self._is_controller = is_controller

    def setup(self):
        super().setup()
        self.context.subscribe_message(self, self.handle_final_term_msg,
                                       lambda c, m: isinstance(c,
                                                               FinalTermination))

    def handle_term_msg(self, content: TerminationMessage,
                        meta: Dict[str, Any]) -> None:
        """Handle the termination message.

        :param content: the message
        :param meta: meta data
        """
        super().handle_term_msg(content, meta)
        if self._is_controller and self._weight_map[content.negotiation_id] \
                == 1:
            logging.info('Agent %s sets termination to True',
                         self.context.aid)
            termination_data = self.context.get_or_create_model(
                TerminationData)
            termination_data.terminated = True
            self.context.update(termination_data)

    def handle_final_term_msg(self, content,
                              meta: Dict[str, Any]) -> None:
        logging.info('Agent %s sets termination to True',
                     self.context.aid)
        termination_data = self.context.get_or_create_model(TerminationData)
        termination_data.terminated = True
        self.context.update(termination_data)
