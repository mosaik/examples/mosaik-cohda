from pathlib import Path
import itertools

import numpy as np

import eocohda.evaluation.util as util

from eocohda.core.evaluator import SpreadingEvaluator, OwnConsumptionEvaluator, PeakDemandCuttingEvaluator
from eocohda.core.market import ListMarket
from eocohda.core.storage import EnergyStorage
from eocohda.core.schedule import Strategy

def own_consumption_test_case(day, n=96, blacklist_add=None):
    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0.1) \
                .self_discharge(0) \
                .capacity(5120) \
                .charge_efficiency(np.sqrt(0.81)) \
                .discharge_efficiency(np.sqrt(0.81)) \
                .max_charge(412.5) \
                .max_discharge(600) \
                .build()
    if Path("data/household/PL1.csv").exists():
        hh_profile = util.read_household_profile([Path("data/household/") / "PL1.csv"], day)[:n]
    else:
        hh_profile = util.read_household_profile([Path("data/household/") / "example.csv"], 0)[:n]

    if Path("data/pv/kronberg.txt").exists():
        pv_profile = util.read_pv_kronberg_by_day(Path("data/pv/") / "kronberg.txt", day)[:n]
    else:
        pv_profile = util.read_pv_kronberg_by_day(Path("data/pv/") / "example.csv", 0)[:n]

    user_values = [0.0002947 for _ in itertools.repeat(None, n)]
    user_market = ListMarket(user_values, user_values)
    input_energy =  (np.array(pv_profile) - np.array(hh_profile)).clip(min=0)
    evaluator = OwnConsumptionEvaluator(user_market, storage, hh_profile, pv_profile, input_list=input_energy, blacklist=[Strategy.BUY, Strategy.SELL] if blacklist_add is None else [Strategy.BUY, Strategy.SELL, blacklist_add])
    return (storage, evaluator)

def spreading_test_case(size_weight=1, blacklist_add=None, n=96):
    """
	Wasserkraftwerk Kirchentellinsfurt (Ortsteil Einsiedel)
    """
    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0) \
                .self_discharge(0) \
                .capacity(6000000*size_weight) \
                .charge_efficiency(np.sqrt(0.8)) \
                .discharge_efficiency(np.sqrt(0.8)) \
                .max_charge(325000*size_weight) \
                .max_discharge(325000*size_weight) \
                .build()
    energy_values = np.array(util.read_smard_market(Path("data/market/") / "spread_test_case_market.csv"))[:n] / 4000000
    evaluator = SpreadingEvaluator(ListMarket(energy_values, energy_values), storage, blacklist=[Strategy.CHARGE, Strategy.OWN_DISCHARGE] if blacklist_add is None else [Strategy.CHARGE, Strategy.OWN_DISCHARGE, blacklist_add])
    return (storage, evaluator)

def peak_cutting(day, size="small", n=96, blacklist_add=None):
    capacity = 177700 if size == "large" else 33300
    max_min_power = 29400 if size == "large" else 8520

    storage = EnergyStorage.builder() \
                .step_size(1) \
                .load(0.1) \
                .self_discharge(0) \
                .capacity(capacity) \
                .charge_efficiency(np.sqrt(0.92)) \
                .discharge_efficiency(np.sqrt(0.92)) \
                .max_charge(max_min_power) \
                .max_discharge(max_min_power) \
                .build()
    hh_profile = util.read_industry_profile(Path("data/industry/") / (size + "_industry.csv"), day)[:n]
    user_values = [0.0001649 for _ in itertools.repeat(None, n)]
    user_market = ListMarket(user_values, user_values)
    evaluator = PeakDemandCuttingEvaluator(user_market, storage, hh_profile, blacklist=[Strategy.CHARGE, Strategy.SELL] if blacklist_add is None else [Strategy.CHARGE, Strategy.SELL, blacklist_add])
    return (storage, evaluator)