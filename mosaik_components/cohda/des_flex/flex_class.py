from typing import List, Tuple, NamedTuple, Optional
from dataclasses import dataclass
from mango.messages.codecs import json_serializable


@json_serializable
@dataclass
class Flexibility:
    # Needed to present flexibility
    flex_max_power: List[float]
    flex_min_power: List[float]
    flex_max_energy_delta: List[float]
    flex_min_energy_delta: List[float]


class ExtendedFlexibility(NamedTuple):
    # Needed to present flexibility
    flex_max_power: List[float]
    flex_min_power: List[float]
    flex_max_energy_delta: List[float]
    flex_min_energy_delta: List[float]

    # Introduced for better visualization, not required to present flexibility
    flex_max_soc: List[float]
    flex_min_soc: List[float]
    max_plan_soc: List[float]
    min_plan_soc: List[float]
