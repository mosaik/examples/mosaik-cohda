"""
Gateway between mosaik and the MAS.

The class :class:`CohdaSimulator` implements the `high-level mosaik API`.

The CohdaSimulator also manages the root main_container for the MAS. It starts
a :class:`MosaikAgent`.  The MosaikAgent serves as a gateway between the
FlexAgents and mosaik.

The FlexAgents do not run within the root main_container but in separate
container in sub processes. These subprocesses are managed by the
MosaikAPI.

The entry point for the MAS is the function :func:`main()`.  It parses the
command line arguments and starts the :func:`run()` coroutine which runs until
mosaik sends a *stop* message to the MAS.

.. _mosaik API:
   https://mosaik.readthedocs.org/en/latest/mosaik-api/low-level.html

"""

import asyncio
import logging
import nest_asyncio
import numpy as np
import mosaik_api_v3 as mosaik_api
from mango import Agent, create_container
from mango.messages.message import Performatives
from mango import RoleAgent
import mango.messages.codecs as codecs
from .mango_library.coalition.core import CoalitionParticipantRole
from .des_flex.flex_class import Flexibility
from .agent_roles import FlexReceiverRole, FlexCohdaRole, \
    FlexTerminationRole, FlexNegotiationStarterRole
from .start_values import StartValues, SolutionSchedule, \
    FinalTermination

logger = logging.getLogger('mosaik.cohda')

def main():
    """
    Run the multi-agent system.
    """
    logging.basicConfig(level=logging.INFO)
    return mosaik_api.start_simulation(Simulator())


# The simulator meta data that is returned in "init()":
META = {
    'api_version': '3.0',
    'type': 'time-based',
    'models': {
        'FlexAgent': {
            'public': True,
            'params': ['control_id',
                       'time_resolution',
                       ],
            'attrs': ['Flexibility',    # input
                      'StartValues',    # input
                      'FlexSchedules',  # output
                      ],
        },
    },
}


class CohdaSimulator(mosaik_api.Simulator):
    """
    Interface of the Cohda Simulator to the Mosaik API.
    """

    def __init__(self):
        """
        :param int step_size: The step size in seconds.
        :param str host: The host address.
        :param int port: The port number.
        :param asyncio.Future stopped: Future to be triggered on "stop()" call.
        :param None or Proxy object mosaik: Proxy object for Mosaik.
        :param None or asyncio event loop loop: Event loop object.
        :param None or str sid: Mosaik simulator ID.
        :param None or Container main_container: Root agent main container.
        :param None or Container agent_container: Container for agents.
        :param None or MosaikAgent mosaik_agent: Mosaik agent object.
        :param dict agents: Dictionary containing agent details.
        :param dict uids: Mapping of agent ID to unit ID.
        :param dict schedules: Schedule data.
        :param dict database: Database to store simulation results.
        :param int time_stamp: Current time stamp.
        :param int iteration_number: Current iteration number.
        """

        super().__init__(META)
        # We have a step size of 15 minutes specified in seconds:
        self.step_size = 60*15  # seconds
        self.host = 'localhost'
        self.port = 5678

        # Set by "run()":
        self.mosaik = None  # Proxy object for mosaik
        self.loop = None

        # Set in "init()"
        self.sid = None  # Mosaik simulator IDs

        # The following will be set in init():
        self.main_container = None  # Root agent main_container
        self.agent_container = None  # Container for agents
        self.mosaik_agent = None

        # Set/updated in "setup_done()"
        self.uids = {}  # agent_id: unit_id
        self.schedules = {}
        self.database = {}
        self.time_stamp = 0
        self.iteration_number = 0

    def init(self, sid, step_size=60*15, time_resolution=1., **sim_params):
        """
        Initializes the simulator.

        :param str sid: Mosaik simulator ID.
        :param float time_resolution: Resolution of time.
        :param sim_params: Additional simulation parameters.
        :return: The simulation metadata.
        """
        
        self.step_size=step_size
        # we have to get the event loop in order to call async functions
        # from init, create and step
        self.loop = sim_params.pop('loop', asyncio.get_event_loop())
        #asyncio.set_event_loop(self.loop)
        self.sid = sid
        # This future will be triggered when mosaik calls "stop()":
        self.stopped = asyncio.Future(loop=self.loop)
        self.loop.run_until_complete(self.create_container_and_main_agents(
            **sim_params))
        return META

    async def create_container_and_main_agents(self, **kwargs):
        """
        Creates the mosaik agent
        """

        # Define serializer for all objects to be sent
        codec = codecs.JSON()
        codec.add_serializer(*Flexibility.__serializer__())
        codec.add_serializer(*StartValues.__serializer__())
        codec.add_serializer(*SolutionSchedule.__serializer__())
        codec.add_serializer(*FinalTermination.__serializer__())

        # Root main_container for the MosaikAgent
        self.main_container = \
            await create_container(addr=(self.host, self.port), codec=codec)

        # Start the MosaikAgent in the main_container
        self.mosaik_agent = MosaikAgent(self.main_container)

        # Create container for the FlexAgents
        self.agent_container = \
            await create_container(addr=(self.host, self.port + 1), codec=codec)

    def create(self, num, model, **model_conf):
        """
        Creates instances of a model and returns a list of entity dictionaries.

        :param int num: Number of instances to create.
        :param str model: Model to create instances of.
        :param model_conf: Model configuration parameters.
        :return: List of entity dictionaries.
        """

        assert model in META['models']
        entities = []
        # Get the number of agents created so far and count from this number
        # when creating new entity IDs:
        n_agents = len(self.mosaik_agent.agents)
        model_conf.setdefault('time_resolution', 1.)
        for i in range(n_agents, n_agents + num):
            # Entity data
            eid = 'Agent_%s' % i
            entities.append({'eid': eid, 'type': model})
            initiator = False
            if i == model_conf['control_id']:
                initiator = True
            agent = self.loop.run_until_complete(self.create_flex_agent(
                self.agent_container, model_conf[
                    'time_resolution'], initiator=initiator))

            # Store details in agents dict
            self.mosaik_agent.agents[eid] = (self.agent_container.addr,
                                             agent.aid)
        return entities

    async def create_flex_agent(self, container,
                                time_resolution, initiator: bool = False) \
            -> Agent:
        """
        Creates a flex agent.

        :param Container container: Agent container.
        :param float time_resolution: Resolution of time.
        :param bool initiator: Initiator status.
        :return: The created agent object.
        """

        a = RoleAgent(container)
        a.add_role(CoalitionParticipantRole())
        a.add_role(FlexCohdaRole(time_resolution=time_resolution))
        a.add_role(FlexReceiverRole((self.main_container.addr,
                                     self.mosaik_agent.aid)))
        a.add_role(FlexTerminationRole(initiator))

        if initiator:
            a.add_role(FlexNegotiationStarterRole())

        return a

    def setup_done(self):
        """
        Executes setup operations once the scenario setup is done.
        """

        full_ids = ['%s.%s' % (self.sid, eid)
                    for eid in self.mosaik_agent.agents.keys()]
        relations = yield self.mosaik.get_related_entities(full_ids)
        
        for full_aid, units in relations.items():
            uid, _ = units.popitem()
            # Create a mapping "agent ID -> unit ID"
            aid = full_aid.split('.')[-1]
            self.uids[aid] = uid

    def finalize(self):
        """
        Finalizes and ends the simulation, storing results in a JSON file.
        """

        self.loop.run_until_complete(
            self._shutdown(self.mosaik_agent,
                           self.main_container, self.agent_container))
        import json
        import os
        file_path = "results_dict.json"
        with open(file_path, 'w') as json_file:
            json.dump(self.database, json_file)

    @staticmethod
    async def _shutdown(*args):
        """
        Shuts down the specified objects.

        :param args: Objects to be shut down.
        """

        futs = []
        for arg in args:
            futs.append(arg.shutdown())
        print('Going to shutdown agents and container... ', end='')
        await asyncio.gather(*futs)
        print('done.')

    def step(self, time, inputs, max_advance):
        """
        Send the flexibility of the controlled units to our agents and
        get new schedules for these units from the agents.

        :param int time: Current time.
        :param dict inputs: Input data.
        :param int max_advance: Maximum advancement time.
        :return: Updated time after step execution.
        """

        debugging = False
        # Prepare input data and forward it to the agents:
        flex_data = {}
        start_data = {}
        for eid, attrs in inputs.items():
            for attr, values in attrs.items():
                assert len(values) == 1  # b/c we're only connected to 1 unit
                _, value = values.popitem()
                if attr == 'StartValues':
                    participant_list = []
                    init_schedules = {}
                    for participant in value.participants:
                        participant_eid = 'Agent_%s' % participant
                        participant_list.append(tuple((
                            self.mosaik_agent.agents[participant_eid])))
                        # needs to be casted to tuple to be useful later on
                        init_schedules[participant_eid] = asyncio.Future(loop=self.loop)

                    start_data[eid] = StartValues(
                        schedule=value.schedule,
                        participants=participant_list)

                    empty_schedule = [0] * len(value.schedule)

                elif attr == 'Flexibility':
                    flex_data[eid] = value
                else:
                    raise AttributeError('Attribute "%s" not available' % attr)

        if not debugging:
            # trigger the loop to enable agents to send / receive messages via
            # run_until_complete
            output_dict = self.loop.run_until_complete(self.mosaik_agent.step(
                input_flex=flex_data, input_start_values=start_data,
                init_schedules=init_schedules, empty_schedule=empty_schedule))

            # Make "set_data()" call back to mosaik to send the set-points:
            self.schedules = {aid: {'FlexSchedules': list(np.array(
                schedule).astype('float64'))} for aid, schedule in
                              output_dict.items()}

        else:
            self.schedules = {aid: {'FlexSchedules': list(np.array(
                empty_schedule).astype('float64'))}
                              for aid, attr in inputs.items()}

        self.time_stamp = time
        return time + self.step_size

    def get_data(self, outputs):
        """
        Retrieves simulation data.

        :param dict outputs: Output specifications.
        :return: Retrieved simulation data.
        """
        self.iteration_number += 1
        data = {}
        data_temp = {}
        for eid, attrs in outputs.items():
            if eid not in self.uids:
                raise ValueError('Unknown entity ID "%s"' % eid)
            data[eid] = {}
            data_temp[eid] = {}
            for attr in attrs:
                if attr != 'FlexSchedules':
                    raise AttributeError('Attribute "%s" not available' % attr)
                data[eid][attr] = self.schedules[eid][attr][int(self.time_stamp/self.step_size)]
                data_temp[eid][attr] = self.schedules[eid][attr]
        self.database[self.time_stamp] = data_temp
        return data


class MosaikAgent(Agent):
    """This agent is a gateway between the mosaik API and the FlexAgents.

    It forwards the current state of the simulated devices to the agents,
    triggers the controller, waits for the controller
    to be done and then collects new set-points for the simulated devices from
    the agents.
    """

    def __init__(self, container):#: Container):
        super().__init__(container)
        self.agents = {}
        self._container = container
        # Done if all new schedules arrived
        self._schedules = {}

    async def step(self, input_flex, input_start_values, init_schedules,
                   empty_schedule):
        """
        This will be called from the mosaik api once per step.

        :param input_flex: the input dict with flexibility from mosaik
        :param input_start_values: the input dict with start values from
        mosaik
        :return: the output dict: {eid_1: schedule, eid_2: schedule}
        """

        # 1. reset
        self.reset(init_schedules)
        logging.info('Reset MAS')

        # 2. update flex for agents
        await self.update_agents(input_flex)
        logging.info('Sent new flex to agents')

        # 3. update start values for agents (automatically start)
        await self.update_agents(input_start_values)
        logging.info('Sent new schedule to agents')

        # 4. get schedules from agents and return it
        schedules = await self.get_schedules(empty_schedule)
        logging.info('Got new schedules from MAS')
        return schedules

    def handle_message(self, content, meta):
        """
        :param content: Content of the message
        :param meta: Meta information
        """

        logging.info('Mosaik agent got message (%s) :%s', meta, content)

        if isinstance(content, SolutionSchedule):
            aid = meta['sender_id']
            for k, value in self.agents.items():
                if value[1] == aid:
                    # Terminiation happens multiple times, therefore catch
                    # for schedules if future is already done
                    if not self._schedules[k].done():
                        self._schedules[k].set_result(content.schedule)
                    logging.info('Got schedule for %s (aid), %s (k)', aid, k)

    async def update_agents(self, data):
        """Update the agents with new data from mosaik."""
        futs = []
        for mosaik_eid, input_data in data.items():
            futs.append(
                self.schedule_instant_task(
                    self._container.send_acl_message(
                        receiver_addr=self.agents[mosaik_eid][0],
                        receiver_id=self.agents[mosaik_eid][1],
                        content=input_data,
                        acl_metadata={'performative': Performatives.inform,
                                      'conversation_id': mosaik_eid,
                                      'sender_id': self.aid,
                                      },
                        
                    )
                )
            )

        await asyncio.gather(*futs)

    async def get_schedules(self, empty_schedule):
        """Collect new FlexSchedules from the agents and return
        them to the mosaik API."""

        # wait for all schedules
        await asyncio.gather(*[fut for fut in self._schedules.values()])

        # return schedule
        all_schedules = {}
        for aid in self.agents.keys():
            if aid in self._schedules.keys():
                all_schedules[aid] = self._schedules[aid].result()
            else:
                all_schedules[aid] = empty_schedule

        return all_schedules

    def reset(self, init_schedules):
        """
        :return: None
        """
        logging.info('Init_schedules=%s', init_schedules)
        self._schedules = init_schedules

class FlexReceiverRoleModified(FlexReceiverRole):
    def setup(self) -> None:
        super().setup()
        self.id = int(self.context.aid.split('agent')[1]) + 1

    def on_change_model(self, model) -> None:
        if model.terminated:
            schedule = self.context.data.current_cohda_solution.solution_candidate.candidate[self.id]
            solution = SolutionSchedule(schedule)
            self.context.schedule_instant_task(
                self.context.send_acl_message(
                    receiver_addr=self._mosaik_agent_address[0],
                    receiver_id=self._mosaik_agent_address[1],
                    content=solution,
                    acl_metadata={'conversation_id':
                                      self._mosaik_agent_address[1],
                                  'sender_id': self.context.aid},
                )
            )

class Simulator(CohdaSimulator):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        nest_asyncio.apply()
        
    def init(self, sid, step_size=60*15, time_resolution=1, **sim_params):
        self.sid = sid
        self.step_size=step_size
        self.loop = sim_params.pop('loop', asyncio.get_event_loop())
        self.host = sim_params.pop('host', 'localhost')
        self.port = sim_params.pop('port', 5678)
        self.stopped = asyncio.Future(loop=self.loop)
        self.loop.run_until_complete(self.create_container_and_main_agents(
            **sim_params))
        return META

    async def create_flex_agent(self, container,
                                time_resolution, 
                                initiator: bool = False) -> Agent:
        a = RoleAgent(container)
        a.add_role(CoalitionParticipantRole())
        a.add_role(FlexCohdaRole(time_resolution=time_resolution))
        a.add_role(FlexReceiverRoleModified((self.main_container.addr,
                                     self.mosaik_agent.aid)))
        a.add_role(FlexTerminationRole(initiator))
        if initiator:
            a.add_role(FlexNegotiationStarterRole())
        return a

    @staticmethod
    async def _shutdown(*args):
        futs = []
        for arg in args:
            futs.append(arg.shutdown())
        await asyncio.gather(*futs)

    def finalize(self):
        try:
            if not self.loop.is_closed():
                self.loop.run_until_complete(
                    self._shutdown(self.mosaik_agent,
                                self.agent_container,
                                self.main_container
                                ))
        except:
            pass

    def step(self, time, inputs, max_advance):
        flex_data = {}
        start_data = {}
        for eid, attrs in inputs.items():
            for attr, values in attrs.items():
                assert len(values) == 1  # b/c we're only connected to 1 unit
                _, value = values.popitem()
                if attr == 'StartValues':
                    participant_list = []
                    init_schedules = {}
                    for participant in value['participants']:
                        participant_eid = 'Agent_%s' % participant
                        participant_list.append(tuple((
                            self.mosaik_agent.agents[participant_eid])))
                        init_schedules[participant_eid] = asyncio.Future(loop=self.loop)

                    start_data[eid] = StartValues(
                        schedule=value['schedule'],
                        participants=participant_list)

                    empty_schedule = [0] * len(value['schedule'])

                elif attr == 'Flexibility':
                    flex_data[eid] = Flexibility(**value)
                else:
                    raise AttributeError('Attribute "%s" not available' % attr)

        output_dict = self.loop.run_until_complete(self.mosaik_agent.step(
                input_flex=flex_data, input_start_values=start_data,
                init_schedules=init_schedules, empty_schedule=empty_schedule))

        self.schedules = {aid: {'FlexSchedules': list(np.array(
                schedule).astype('float64'))} for aid, schedule in
                              output_dict.items()}

        self.time_stamp = time
        return time + self.step_size


if __name__ == '__main__':
    main()
