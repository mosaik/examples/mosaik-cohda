# Project was moved to https://gitlab.com/hybit1/mosaik-cohda
## Welcome to *mosaik-cohda*!

*mosaik-cohda* introduces COHDA as a simulator for a mosaik co-simulation. It implements algorithms and models for generating multi-criteria optimized schedules using COHDA negotiation protocol. COHDA was proposed by Christian Hinrichs et al. (2014), see paper [*one*](https://www.inderscience.com/offers.php?id=85895) and [*two*](https://uol.de/f/2/dept/informatik/ag/ui/publications/HLS14b.pdf).

This version has been [forked](https://gitlab.com/zdin-zle/models/mosaik-cohda/-/tree/810f903d) and adapted from the [ZDIN-ZLE project](https://gitlab.com/zdin-zle/models/mosaik-cohda). The project integrates:

- [*mango*](https://gitlab.com/mango-agents/mango), which is a python library for multi-agent system modeling.
- The flexibility model proposed by [Ulbig and Andersson (2012)](https://ieeexplore.ieee.org/document/6344676), see this [implementation](https://gitlab.com/digitalized-energy-systems/models/flex/ulbig).
- [Flex-COHDA](https://gitlab.com/digitalized-energy-systems/models/flex-cohda/-/tree/1ae16691bc9c1f8c8dd42021f50228d050b1e13a) — COHDA implementation for flexibility optimization.
- [EO-COHDA](https://gitlab.com/digitalized-energy-systems/models/eo-cohda) — Energy storage optimization based on [ISAAC](https://github.com/mtroeschel/isaac)-COHDA.


In this version we have implemented a *Simulator()* class that inherits the original *CohdaSimulator()*, but overrides some of the original methods to make it compatible with the updated *mango* and *mosaik*. Documentation for the original *CohdaSimulator()* can be found in the ZDIN-ZLE project [repo](https://gitlab.com/zdin-zle/models/mosaik-cohda/-/tree/810f903d). **Note** that this implementation does not impose any additional guarantees or restrictions on the generated solutions.


## Installation

```
pip install mosaik-cohda
```

If you don't want to install this through PyPI, you can use pip to install the requirements.txt file:
```
pip install -r requirements.txt # To use this, you have to install at least version 3.2.0 of mosaik.
```

Tests can be executed from cloned repo folder using *pytest*:
```
pytest tests
```

## Usage

**Note** that you can check out the working example in the *tests* folder.

Specify simulators configurations within your scenario script:
```python
sim_config = {
   'COHDA': {
               'python': 'mosaik_components.cohda:Simulator'
            },
    # ...
}
```

Initialize the simulator:
```python
world = mosaik.World(sim_config)
cohda_sim = world.start('COHDA', step_size=STEP_SIZE)
```

Instantiate agents:
```python
n_agents = 5
cohda = cohda_sim.FlexAgent.create(n_agents, 
                                    **{'control_id': 0})
# control_id contains the index of the agent initiating the negotiation
```

Connect and run:
```python
for agent in range(n_agents):
   world.connect(input_target[0], 
                  cohda[agent], 
                  ("value", "StartValues"))
   world.connect(input_flexibility[0], 
                  cohda[agent], 
                  ("value", "Flexibility"))

world.run(until=END)
```

## Authors & Acknowledgments

- Fernando Andres Penaherrera Vaca (<fernando.andres.penaherrera.vaca@offis.de>)
- Jan Philipp Hörding (<jan.philipp.hoerding@offis.de>)
- Stephan Ferenz (<stephan.ferenz@offis.de>)
- Rico Schrage (<rico.schrage@offis.de>)
- Updated and isolated version was prepared by Danila Valko (<mosaik@offis.de>)

