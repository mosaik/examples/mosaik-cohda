import random
import time
import logging
import numpy as np
from mosaik_components.cohda import Simulator

import warnings
warnings.filterwarnings("ignore", category=FutureWarning)
warnings.filterwarnings("ignore", category=DeprecationWarning)

def test_cohda():
        """
        Run test.
        """

        seed = 13
        n_agents = 3
        logging.basicConfig(level=logging.DEBUG)
        random.seed(seed)
        np.random.seed(seed)
        host = 'localhost'
        base_port = 7000
        step_size = 60*15
        time_resolution = 1.
        id = random.randint(1, 10)
          
        simulator = Simulator()
        simulator.init(sid=id, step_size=step_size, 
                       time_resolution=time_resolution,
                       host=host, port=base_port+id)
        simulator.create(n_agents, 'FlexAgent', **{'control_id': 0})
        simulator.setup_done()

        target_schedule = [0.5, 2.0, 5.0]
        flexibility = {'flex_max_power': [3.0, 3.0, 3.0],
            'flex_min_power': [0.0, 0.0, 0.3],
            }
    
        input_data = {}
        output_data = {}
        for eid in simulator.mosaik_agent.agents.keys():
                flex_max_energy_delta = flexibility.get('flex_max_energy_delta', [100] * len(flexibility['flex_max_power']))
                flex_min_energy_delta = flexibility.get('flex_min_energy_delta', [0] * len(flexibility['flex_min_power']))
                input_data.setdefault(eid, {'StartValues': {'ID_0': {'schedule': target_schedule, 
                                                                      'participants': list(range(n_agents))}},
                                            'Flexibility': {'ID_0': dict(flex_max_power=flexibility['flex_max_power'],
                                                                         flex_min_power=flexibility['flex_min_power'],
                                                                         flex_max_energy_delta=flex_max_energy_delta,
                                                                         flex_min_energy_delta=flex_min_energy_delta)}
                                             })

                simulator.uids[eid] = None # since there are no real connections, manually activate all agents
                output_data.setdefault(eid, ['FlexSchedules'])

        simulator.step(time=900, inputs=input_data, max_advance=0)
        time.sleep(1)
        output_data = simulator.get_data(outputs=output_data) # returns schedule with index int(900/step_size)
        simulator.finalize()

        print('n_agents:', n_agents)
        print('target_schedule:', target_schedule)
        print('flexibility:', flexibility)
        print('schedules:', simulator.schedules)
        print('output_data:', output_data)

        assert len(output_data) == n_agents
        assert len(simulator.schedules['Agent_0']['FlexSchedules']) == len(target_schedule)
        assert output_data['Agent_0']['FlexSchedules'] == simulator.schedules['Agent_0']['FlexSchedules'][1] 
        assert output_data['Agent_1']['FlexSchedules'] == simulator.schedules['Agent_1']['FlexSchedules'][1]

        print()
        solution = np.asarray([i['FlexSchedules'] for i in simulator.schedules.values()]).sum(0)
        print('solution:', solution)
        rmse = np.sqrt(np.sum((target_schedule - solution)**2)/len(target_schedule))
        print('RMSE:', rmse)
        assert rmse < max(target_schedule)


if __name__ == '__main__':
    test_cohda()

