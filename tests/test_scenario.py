import logging
import numpy as np
import mosaik

import warnings
warnings.filterwarnings("ignore", category=FutureWarning)
warnings.filterwarnings("ignore", category=DeprecationWarning)

def test_scenario():
        """
        Run test.
        """
        logging.basicConfig(level=logging.DEBUG)
        sim_config = {
            'Input': {
                'python': 'mosaik.basic_simulators.input_simulator:InputSimulator',
            },
            'Output': {
                'python': 'mosaik.basic_simulators.output_simulator:OutputSimulator'
            },
            'COHDA': {
                'python': 'mosaik_components.cohda:Simulator'
            },
        }

        n_agents = 3
        target_schedule = {'schedule': [0.5, 2.0, 5.0],
                        'participants': list(range(n_agents)),
                        }
        flexibility = {'flex_max_power': [3.0, 3.0, 3.0],
                    'flex_min_power': [0.0, 0.0, 0.3],
                    'flex_min_energy_delta': [0.0, 0.0, 0.0],
                    'flex_max_energy_delta': [100.0, 100.0, 100.0],
                    }

        STEP_SIZE = 60 * 15
        END = STEP_SIZE * len(target_schedule['schedule'])

        # Set up the "world" of the scenario.
        world = mosaik.World(sim_config)

        # Initialize the simulators.
        input_sim = world.start('Input', step_size=STEP_SIZE)
        cohda_sim = world.start('COHDA', step_size=STEP_SIZE, host='localhost', port=8000)
        output_sim = world.start('Output')

        # Create entities
        input_target = input_sim.Constant.create(1, constant=target_schedule)
        input_flexibility = input_sim.Constant.create(1, constant=flexibility)
        cohda = cohda_sim.FlexAgent.create(n_agents, **{'control_id': 0})
        output = output_sim.Dict.create(n_agents)

        # Connecting everything
        for agent in range(n_agents):
            world.connect(input_target[0], cohda[agent], ("value", "StartValues"))
            world.connect(input_flexibility[0], cohda[agent], ("value", "Flexibility"))
            world.connect(cohda[agent], output[agent], "FlexSchedules")
            
        #Run and print
        output_dicts = [output_sim.get_dict(output[i].eid) for i in range(n_agents)]
        world.run(until=END)

        print('n_agents:', n_agents)
        print('target_schedule:', target_schedule)
        print('flexibility:', flexibility)
        print('output_dicts:', output_dicts)
        schedules = [[list(j['FlexSchedules'].values())[0] 
                                for j in i.values()] 
                                            for i in output_dicts]
        print()
        print('schedules:', schedules)
        assert len(schedules) == n_agents
                
        target = target_schedule['schedule']
        solution = np.asarray(schedules).sum(0)
        print('target:', target)
        print('solution:', solution)
        rmse = np.sqrt(np.sum((target - solution)**2)/len(target))
        print('RMSE:', rmse)
        assert rmse < max(target)


if __name__ == '__main__':
    test_scenario()


